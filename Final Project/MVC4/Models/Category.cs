﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC4.Models
{
    public class Category
    {
        [Required]
        public int ID { get; set; }
        
        [Required(ErrorMessage="Tên thể loại không được để trống!")]
        [System.ComponentModel.DisplayName("Tên thể loại")]
        [MinLength(3, ErrorMessage = "Tối thiểu 3 kí tự")]
        public String Title { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }
}