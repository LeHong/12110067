﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC4.Models
{
    public class Post
    {
        [Required]
        public int ID { get; set; }
        [Required(ErrorMessage="Tiêu đề không được trống!")]
        [System.ComponentModel.DisplayName("Tiêu đề")]
        public String Title { get; set; }
        [Required(ErrorMessage = "Nội dung không được trống!")]
        [System.ComponentModel.DisplayName("Nội dung")]
        public String Body { get; set; }
        [System.ComponentModel.DisplayName("Ngày tạo")]
        public DateTime Ngaytao { get; set; }
        [System.ComponentModel.DisplayName("Nội chỉnh sửa")]
        public DateTime Ngaychinhsua { get; set; }

        public virtual UserProfile UserProfile { get; set; }
        [System.ComponentModel.DisplayName("Tác giả")]
        public int UserProfileUserId { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }

        public int CateID { get; set; }
        public virtual Category Categorys { get; set; }
    }
}