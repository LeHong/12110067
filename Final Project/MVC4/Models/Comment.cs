﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC4.Models
{
    public class Comment
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Chưa bình luận")]
        [System.ComponentModel.DisplayName("Bình luận")]
        public String Body { get; set; }
        [System.ComponentModel.DisplayName("Ngày tạo")]
        public DateTime Ngaytao { get; set; }
        [System.ComponentModel.DisplayName("Ngày sử chữa")]
        public DateTime Ngaysuchua { get; set; }

        public string LastTime
        {
            get
            {

                if ((DateTime.Now - Ngaytao).Days > 1)
                    return (DateTime.Now - Ngaytao).Days.ToString() + " ngày";
                else if ((DateTime.Now - Ngaytao).Hours > 1)
                    return (DateTime.Now - Ngaytao).Hours.ToString() + " giờ";
                else if ((DateTime.Now - Ngaytao).Minutes > 1)
                    return (DateTime.Now - Ngaytao).Minutes.ToString() + " phút";
                else
                    return (DateTime.Now - Ngaytao).Seconds.ToString() + " giây";
            }
        }
        public int PostID { get; set; }
        public virtual Post Posts { get; set; }

    }
}