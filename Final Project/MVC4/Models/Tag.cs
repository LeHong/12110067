﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC4.Models
{
    public class Tag
    {
        public int TagID { get; set; }
        [System.ComponentModel.DisplayName("Tag")]
        public String Content { get; set; }


        public virtual ICollection<Post> Posts { get; set; }
    }
}