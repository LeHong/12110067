﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog23_10.Models
{
    [Table("BaiViet")]
    public class Post
    {
        public int ID { set; get; }
        [Required(ErrorMessage="Vui lòng nhập dữ liệu")]
        [StringLength(500, ErrorMessage = "Số lượng kí tự từ 20 - 500", MinimumLength=20)]
        public String Title { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập dữ liệu")]
        [MinLength(50, ErrorMessage="Số lượng kí tự ít nhất là 50")]
        public String Body { set; get; }
        [DataType(DataType.Date, ErrorMessage="Nhập dữ liệu kiểu ngày tháng")]
        public DateTime DateCreated { set; get; }
        [DataType(DataType.Date,ErrorMessage="Nhập dữ liệu kiểu ngày tháng")]
        public DateTime DateUpdate { set; get; }


        public virtual ICollection<Comment> Comments { set; get; } // mot Post co nhieu Comment

        public virtual ICollection<Tag> Tags { set; get;}

        // AccountID lien ket voi bang Account
        [Required]
        public int AccountID { set; get; } 
        public virtual Account Account { set; get; }
    }
}