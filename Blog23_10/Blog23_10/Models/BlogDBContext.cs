﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Blog23_10.Models
{
    public class BlogDBContext:DbContext
    {
        public DbSet<Post> Posts { set; get; } // anh xa tu table len object
        public DbSet<Comment> Comments { set; get; }
        public DbSet<Tag> Tags { set; get; }
        public DbSet<Account> Accounts { set; get; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Post>()
                .HasMany(d => d.Tags).WithMany(t => t.Posts)
                .Map(p => p.MapLeftKey("PostID").MapRightKey("TagID").ToTable("Tag_Post"));
        }
    }
}