﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog23_10.Models
{
    public class Comment
    {
        public int ID {set;get;}
        [Required(ErrorMessage = "Vui lòng nhập dữ liệu")]
        [MinLength(50, ErrorMessage="Số lượng kí tự ít nhất là 50")]
        public String Body { set; get; }
        [DataType(DataType.Date,ErrorMessage="Nhập dữ liệu kiểu ngày tháng")]
        public DateTime DateCreated { set; get; }
        [DataType(DataType.Date,ErrorMessage="Nhập dữ liệu kiểu ngày tháng")]
        public DateTime DateUpdate { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập dữ liệu")]
        public String Author { set; get; }

        public int PostID { set; get; } // PostID lien ket voi bang Post
        public virtual Post Post { set; get; } // mot Comment co mot Post
    }
}