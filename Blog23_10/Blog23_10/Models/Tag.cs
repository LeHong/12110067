﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog23_10.Models
{
    public class Tag
    {
        public int TagID { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập dữ liệu")]
        [StringLength(100, ErrorMessage="Số lượng kí tự từ 10 - 100", MinimumLength=10)]
        public String Content { set; get;}

        public virtual ICollection<Post> Posts { set; get; }
    }
}