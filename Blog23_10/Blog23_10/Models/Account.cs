﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog23_10.Models
{
    public class Account
    {
       
        public int AccountID { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập dữ liệu")]
        [DataType(DataType.Password)]
        public String Password { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập dữ liệu")]
        [DataType(DataType.EmailAddress, ErrorMessage="Email không hợp lệ")]
        public String Email { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập dữ liệu")]
        [MaxLength(100, ErrorMessage = "FirstName có tối đa 100 kí tự")]
        public String FirstName { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập dữ liệu")]
        [MaxLength(100, ErrorMessage = "LastName có tối đa 100 kí tự")]
        public String LastName { set; get; }

        public virtual ICollection<Post> Posts { set; get;}

    }
}