namespace Blog23_10.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan61 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BaiViet", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.BaiViet", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.Comments", "Author", c => c.String(nullable: false));
            AlterColumn("dbo.Tags", "Content", c => c.String(maxLength: 100));
            AlterColumn("dbo.Accounts", "Password", c => c.String(nullable: false));
            AlterColumn("dbo.Accounts", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.Accounts", "FirstName", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Accounts", "LastName", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Accounts", "LastName", c => c.String());
            AlterColumn("dbo.Accounts", "FirstName", c => c.String());
            AlterColumn("dbo.Accounts", "Email", c => c.String());
            AlterColumn("dbo.Accounts", "Password", c => c.String());
            AlterColumn("dbo.Tags", "Content", c => c.String());
            AlterColumn("dbo.Comments", "Author", c => c.String());
            AlterColumn("dbo.BaiViet", "Body", c => c.String());
            AlterColumn("dbo.BaiViet", "Title", c => c.String());
        }
    }
}
