namespace Blog23_10.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan82 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BaiViet", "ID", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Comments", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.Tags", "Content", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tags", "Content", c => c.String(maxLength: 100));
            AlterColumn("dbo.Comments", "Body", c => c.String());
            AlterColumn("dbo.BaiViet", "ID", c => c.Int(nullable: false));
        }
    }
}
