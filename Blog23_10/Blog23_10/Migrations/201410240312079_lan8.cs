namespace Blog23_10.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan8 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BaiViet", "ID", c => c.Int(nullable: false));
            AlterColumn("dbo.BaiViet", "Title", c => c.String(maxLength: 500));
            AlterColumn("dbo.BaiViet", "Body", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BaiViet", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.BaiViet", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.BaiViet", "ID", c => c.Int(nullable: false, identity: true));
        }
    }
}
