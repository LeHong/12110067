﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Blog4.Models
{
    public class Comment
    {
        public int ID { set; get; }
        
        [Required(ErrorMessage = "Vui lòng nhập dữ liệu")]
        //[MinLength(50, ErrorMessage = "Số lượng kí tự ít nhất là 50")]
        public String Body { set; get; }
        
        [DataType(DataType.DateTime, ErrorMessage = "Nhập dữ liệu kiểu ngày tháng")]
        public DateTime DateCreated { set; get; }
        
        [DataType(DataType.DateTime, ErrorMessage = "Nhập dữ liệu kiểu ngày tháng")]
        public DateTime DateUpdate { set; get; }
        
        [Required(ErrorMessage = "Vui lòng nhập dữ liệu")]
        public String Author { set; get; }

        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Minutes;
            }
        }//

        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}