﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace Blog4.Models
{
    public class BlogDbContext : DbContext
    {
        public BlogDbContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Post> Posts { set; get; }

        public DbSet<Tag> Tags { set; get; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Post>().HasMany(t => t.Tags)
                .WithMany(p => p.Posts).Map(k => k.MapLeftKey("PostID")
                    .MapRightKey("TagID").ToTable("Tag_Post"));
        }
        public DbSet<Comment> Comments { set; get; }

    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }

        public virtual ICollection<Post> Posts { set; get; }
    }

    public class RegisterExternalLoginModel
    {
        [Required(ErrorMessage = "Vui lòng nhập liệu.")]
        [Display(Name = "Tên User")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required(ErrorMessage = "Vui lòng nhập liệu.")]
        [DataType(DataType.Password)]
        [Display(Name = "Password hiện tại")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập liệu.")]
        [StringLength(100, ErrorMessage = "Password ít nhất 6 kí tự.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password mới")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Nhập lại password")]
        [Compare("NewPassword", ErrorMessage = "Mật khẩu không khớp. Vui lòng thử lại.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required(ErrorMessage = "Vui lòng nhập liệu.")]
        [Display(Name = "Tên User")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập liệu.")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Nhớ mật khẩu?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required(ErrorMessage = "Vui lòng nhập liệu.")]
        [Display(Name = "Tên User")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập dữ liệu.")]
        [StringLength(100, ErrorMessage = "Password ít nhất 6 kí tự.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Nhập lại password")]
        [Compare("Password", ErrorMessage = "Mật khẩu không khớp. Vui lòng thử lại.")]
        public string ConfirmPassword { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
