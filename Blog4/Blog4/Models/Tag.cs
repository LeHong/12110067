﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog4.Models
{
    public class Tag
    {
        public int TagID { set; get; }
        
        [Required(ErrorMessage = "Vui lòng nhập dữ liệu")]
        
        public String Content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}